import itertools

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import auc, roc_curve, accuracy_score

# confusion matrix code from Maurizio
# /eos/user/m/mpierini/DeepLearning/ML4FPGA/jupyter/HbbTagger_Conv1D.ipynb
def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    fig, ax = plt.subplots(1,1,figsize=(6,6))
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    # plt.title(title)
    cbar = plt.colorbar()
    plt.clim(0, 1)
    cbar.set_label(title)
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.0
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt), horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

    # plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    return fig


def plotRoc(fpr, tpr, auc, labels, linestyle, legend=True):
    fig, ax = plt.subplots(1,1,figsize=(6,6))
    for _i, label in enumerate(labels):
        plt.plot(
            tpr[label],
            fpr[label],
            label='{} tagger, AUC = {:.1f}%'.format(label.replace('j_', ''), auc[label] * 100.0),
            linestyle=linestyle,
        )
    plt.semilogy()
    plt.xlabel("Signal Efficiency")
    plt.ylabel("Background Efficiency")
    plt.ylim(0.001, 1)
    plt.grid(True)
    if legend:
        plt.legend(loc='upper left')
    plt.figtext(0.25, 0.90, 'hls4ml', fontweight='bold', wrap=True, horizontalalignment='right', fontsize=14)
    return fig


def rocData(y, predict_test, labels):
    df = pd.DataFrame()

    fpr = {}
    tpr = {}
    auc1 = {}

    for i, label in enumerate(labels):
        df[label] = y[:, i]
        df[label + '_pred'] = predict_test[:, i]

        fpr[label], tpr[label], threshold = roc_curve(df[label], df[label + '_pred'])

        auc1[label] = auc(fpr[label], tpr[label])
    return fpr, tpr, auc1

def makeRoc(y, predict_test, labels, linestyle='-', legend=True):
    if 'j_index' in labels:
        labels.remove('j_index')

    fpr, tpr, auc1 = rocData(y, predict_test, labels)
    fig = plotRoc(fpr, tpr, auc1, labels, linestyle, legend=legend)
    return fig

def all_evaluations(model, X_test, y_test, labels, working_points=[1e-5,1e-4,1e-3,1e-2]):
  y = model.predict(X_test)
  accuracy = accuracy_score(np.argmax(y, axis=1), np.argmax(y_test, axis=1))
  fpr, tpr, auc = rocData(y_test, y, labels)
  fig_roc = plotRoc(fpr, tpr, auc, labels, '-')
  return Metrics(fig_roc, accuracy, auc, Metrics.compute_working_points(fpr, tpr, working_points))

class Metrics:
    def __init__(self,
                 roc : plt.figure,
                 accuracy : float,
                 auc : dict,
                 working_points : pd.DataFrame):
        self.roc = roc
        self.accuracy = accuracy
        auc_copy = {}
        for k, v in auc.items():
            auc_copy[k] = [v]
        self.auc = pd.DataFrame(auc_copy)
        self.working_points = working_points
    
    def log_mlflow(self):
        import mlflow
        mlflow.log_figure(self.roc, "roc.png")
        mlflow.log_table(self.auc, 'auc.json')
        mlflow.log_table(self.working_points, 'working_point.json')

    def compute_working_points(fpr, tpr, working_points=[1e-5,1e-4,1e-3,1e-2]):
        table = {'fpr' : []}
        for key in fpr.keys():
            table[key] = []
        for working_point in working_points:
            table['fpr'].append(working_point)
            for key in fpr.keys():
                table[key].append(tpr[key][np.argwhere(fpr[key] <= working_point)[-1][0]])
        return pd.DataFrame(table)

if __name__ == '__main__':
  from args import get_common_parser, handle_common_args
  parser = get_common_parser()