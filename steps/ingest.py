from sklearn.datasets import fetch_openml
from sklearn.preprocessing import LabelEncoder
import numpy as np
from tensorflow.keras.utils import to_categorical
import os
import pickle
import logging
logger = logging.getLogger(__name__)

def fetch_data(save=False, output_dir='./'):
  logger.info('Fetching OpenML dataset')
  data = fetch_openml('hls4ml_lhc_jets_hlf')
  X, y = data['data'], data['target']
  le = LabelEncoder()
  y = le.fit_transform(y)
  y = to_categorical(y, 5)
  if save:
    if not os.path.exists(f'{output_dir}/data'):
      os.makedirs(f'{output_dir}/data')
    np.save(f'{output_dir}/data/X.npy', X)
    np.save(f'{output_dir}/data/y.npy', y)
    with open(f'{output_dir}/data/label_encoder.pkl', 'wb') as f:
      pickle.dump(le, f)
  return X, y

if __name__ == '__main__':
  from args import get_common_parser, handle_common_args
  parser = get_common_parser()
  args = parser.parse_args()
  handle_common_args(args)
  logger.setLevel(logging.DEBUG)
  fetch_data(args.save, args.workdir)
