from sklearn.preprocessing import StandardScaler
import numpy as np
import pickle
import logging
logger = logging.getLogger(__name__)

def scale(X, save=False, workdir='./'):
  logger.info('Scaling with standard scaler')
  scaler = StandardScaler()
  X_transform = scaler.fit_transform(X)
  if save:
    np.save(f'{workdir}/X_transformed.npy', X_transform)
    with open(f'{workdir}/scaler.pkl', 'wb') as f:
      pickle.dump(scaler, f)

def load_data(workdir='./'):
  X = np.load(f'{workdir}/X.npy')
  y = np.load(f'{workdir}/y.npy')
  return X, y

def load_train_data(workdir='./'):
  X_train = np.load(f'{workdir}/data/train/X_transformed.npy')
  y_train = np.load(f'{workdir}/data/train/y.npy')
  return X_train, y_train

def load_test_data(workdir='./'):
  X_test = np.load(f'{workdir}/data/test/X_transformed.npy')
  y_test = np.load(f'{workdir}/data/test/y.npy')
  return X_test, y_test

if __name__ == '__main__':
  from args import get_common_parser
  parser = get_common_parser()
  parser.add_argument('--test-size', dest='test_size', default=0.25)
  args = parser.parse_args()
  X_train, y_train = load_data(f'{args.workdir}/data/train/')
  scale(X_train, args.save, f'{args.workdir}/data/train/')
  X_test, y_test = load_data(f'{args.workdir}/data/test/')
  scale(X_test, args.save, f'{args.workdir}/data/test/')
