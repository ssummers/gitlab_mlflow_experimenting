from sklearn.model_selection import train_test_split
import numpy as np
import os
import logging
logger = logging.getLogger(__name__)

def load(workdir : str):
  X = np.load(f'{workdir}/data/X.npy')
  y = np.load(f'{workdir}/data/y.npy')
  return X, y

def split(X, y, test_size, save=False, output_dir='.'):
  logger.info(f'Splitting with test_size {test_size}')
  X_train_val, X_test, y_train_val, y_test = train_test_split(X, y, test_size=test_size)
  if save:
    if not os.path.exists(f'{output_dir}/data/train/'):
      os.makedirs(f'{output_dir}/data/train/')
    if not os.path.exists(f'{output_dir}/data/test/'):
      os.makedirs(f'{output_dir}/data/test/')
    np.save(f'{output_dir}/data/train/X.npy', X_train_val)
    np.save(f'{output_dir}/data/train/y.npy', y_train_val)
    np.save(f'{output_dir}/data/test/X.npy', X_test)
    np.save(f'{output_dir}/data/test/y.npy', y_test)
  return X_train_val, X_test, y_train_val, y_test

if __name__ == '__main__':
  from args import get_common_parser
  parser = get_common_parser()
  parser.add_argument('--test-size', dest='test_size', default=0.25)
  args = parser.parse_args()
  X, y = load(args.workdir)
  split(X, y, args.test_size, args.save, args.workdir)
  